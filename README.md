
# Collisions

Bouncing balls by [Corpsman](http://corpsman.de/index.php?doc=beispiele/pingpong), ported to Lazarus.

BGRABitmap version by [circular17](http://www.developpez.net/forums/showthread.php?t=1476320#post8309416). See also [this discussion](https://forum.lazarus.freepascal.org/index.php/topic,54207.0.html).

![screenshot](screenshot/collisions.png)
